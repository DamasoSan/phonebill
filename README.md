# Phone Bill #

The program has a menu where you can choose whether to read the calls with a file (Recommended) or for the user to enter the calls by keyboard.

The file where the calls must be entered must be in the root and must be called "bill.txt"

The code can be found in the following path: PhoneBill\src\main\scala

### Developed by: Dámaso Sánchez Arenas ###